const Koa = require('koa');
const mongoose = require('mongoose');
const app = new Koa();

//router
const Router = require('koa-router');

//父路由
const router = new Router();

//bodyparse:该中间件用于post请求的数据
const bodyParser = require('koa-bodyparser');
app.use(bodyParser());

//引入数据库操作方法
const users = require('./routers/api/users');

//checkToken作为中间件存在
const checkToken = require("./token/checkToken")

//登录
const loginRouter = new Router();
loginRouter.post('/login',users.Login);

//注册
const registerRouter = new Router();
registerRouter.post('/register',users.Reg);

//获取所有用户
const userRouter = new Router();
// userRouter.get('/user',checkToken,users.GetAllUsers);
userRouter.post('/user', checkToken, users.GetAllUsers);

//修改某个用户
const updateUserRouter = new Router();
updateUserRouter.post('/updateUser', checkToken, users.UpdateUser);

//删除某个用户
const delUserRouter = new Router();
delUserRouter.post('/delUser', checkToken, users.DelUser);

//装载上面四个子路由
router.use('/api',loginRouter.routes(),loginRouter.allowedMethods());
router.use('/api',registerRouter.routes(),registerRouter.allowedMethods());
router.use('/api', userRouter.routes(), userRouter.allowedMethods());
router.use('/api', updateUserRouter.routes(), updateUserRouter.allowedMethods());
router.use('/api',delUserRouter.routes(),delUserRouter.allowedMethods());


//加载路由中间件
app.use(router.routes()).use(router.allowedMethods());


const db = require('./config/keys').mongoURI;
mongoose.connect(db, { useNewUrlParser: true })
        .then(() => console.log('MongoDB connected'))
        .catch(err => console.log(err))


const port = process.env.PORT || 5000;

app.listen(port, () => { 
    console.log(`Server running on port ${port}`);
})