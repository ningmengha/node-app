const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InfoSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    shortname: {
        type: String
    },
    sex: {
        type: String,
        required: true
    },
    data: {
        type: Date,
        default: Date.now
    }
})
module.exports = Info = mongoose.model('infos', InfoSchema)