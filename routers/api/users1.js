const express = require('express');
const router = express.Router();
//用户表
const User = require('../../models/User');
//信息表
const Info = require('../../models/Info')

//createToken
const createToken = require('../../token/createToken');

router.get('/test', (req, res) => {
    res.send("hello world!");
})

//register
router.post('/register', (req, res) => {
    User.findOne({email:req.body.email})
    .then(user => {
        if (user) {
            return res.status(200).json({
                retCode: 'FAILURE',
                message: '邮箱已注册'
            })
        }else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            })
            newUser.token = createToken(req.body.email);
            newUser.save()
                .then(user => res.status(200).json({
                    retCode: 'SUCCESS',
                    message: '注册成功'
                }))
                .catch(err => console.log(err));
        }
    })
})

//login
router.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({
        email: req.body.email
    })
        .then(user => {
            if(!user) {
                return res.status(200).json({
                    retCode: 'FAILURE',
                    message: '用户不存在'
                })
            }
            if (password == user.password) {
                let token = createToken(email);
                user.token = token;
                user.save()
                    .then(user => {
                        res.status(200).json({
                            retCode: 'SUCCESS',
                            email,
                            token,
                            message: '登录成功'
                        })
                    })
                    .catch(err => console.log(err));
            } else {
                return res.status(200).json({
                    retCode: 'FAILURE',
                    message: '密码错误'
                })
            }
        })
})
router.get('/alluser', (req, res) => {
    User.find({})
        .then(data => { 
            if (data) {
                return res.status(200).json({
                    retCode: 'SUCCESS',
                    data: data
                })
            } else { 
                return res.status(200).json({
                    retCode: 'FAILURE',
                    message: '请求失败'
                })
            }
        })
})

module.exports = router;