//用户表
const mongoose = require('mongoose');
const User = require('../../models/User');

//createToken
const createToken = require('../../token/createToken');

//数据库的操作
//根据用户邮箱查找用户
const findUser = (email) => {
    return new Promise((resolve, reject) => {
        User.findOne({email}, (err,doc) => {
            if(err) {
                reject(err);
            }
            resolve(doc);
        })
    })
}

//找到符合条件的所有用户
const findAllUsers = (conditions) => {
    return new Promise((resolve, reject) => {
        
        let pageSize = Number(conditions.rows || '10');
        let currentPage = Number(conditions.page || '1');
        let sort = {'date': -1};
        let condition = conditions.name ? {name: conditions.name}: {};
        let skipnum = (currentPage -1) * pageSize;
        
        User.countDocuments(condition,(err,count) => {
            if(err) {
                reject(err);
            }
            let doc = {};
            doc.count = count;
            User.find(condition)
            .skip(skipnum)
            .limit(pageSize)
            .sort(sort)
            .exec((err, res) => {
                if(err) {
                    reject(err);
                }
                doc.list = res;
                resolve(doc);
            })
        })
    })
}
//修改某个用户
const updateUser = (wherestr, updatestr) => {
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate(wherestr, updatestr, (err, doc) => {
            if (err) {
                reject(err);
            }
            //修改用户成功
            resolve(doc);
        })
    })
}
//删除某个用户
const delUser = (id) => {
    return new Promise((resolve, reject) => {
        User.findOneAndRemove({_id:id},err => {
            if(err) {
                reject(err);
            }
            //删除用户成功
            resolve();
        })
    })
}

//登录
const Login = async(ctx) => {
    //拿到邮箱密码
    let email = ctx.request.body.email;
    let password = ctx.request.body.password;
    let doc = await findUser(email);
    if(!doc) {
        //用户不存在
        ctx.status = 200;
        ctx.body = {
            retCode: 'FAILURE',
            message: '用户不存在'
        }
    }else if(doc.password === password) {
        //密码相同
        //生成一个新的token，并存到数据库
        let token = createToken(email);
        doc.token = token;
        await new Promise((resolve, reject) => {
            doc.save((err) => {
                if(err) {
                    reject(err);
                }
                resolve();
            })
        })
        ctx.status = 200;
        ctx.body = {
            retCode: 'SUCCESS',
            email,
            token,
            message: '登录成功'
        }
    }else {
        //密码错误
        ctx.status = 200;
        ctx.body = {
            retCode: 'FAILURE',
            message: '密码错误'
        }
    }
}

// 注册
const Reg = async (ctx) => {
    let user = new User({
        email : ctx.request.body.email,
        password : ctx.request.body.password,
        name: ctx.request.body.name,
        sex: ctx.request.body.sex,
        phone: ctx.request.body.phone,
        token :  createToken(this.email)
    })
    let doc = await findUser(user.email);
    if(doc) {
        //邮箱已注册
        ctx.status = 200,
        ctx.body = {
            retCode: 'FAILURE',
            message: '邮箱已注册'
        }
    }else {
        await new Promise((resolve, reject) => {
            user.save((err) => {
                if(err) {
                    reject(err);
                }
                resolve();
            })
        })
        //注册成功
        ctx.status = 200;
        ctx.body = {
            retCode: 'SUCCESS',
            message: '注册成功'
        }
    }
}

//获取所有用户信息
const GetAllUsers = async (ctx) => {
    //查询所有用户信息;
    
    let query = ctx.request.body;
    let doc = await findAllUsers(query);

    ctx.status = 200;
    ctx.body = {
        retCode: 'SUCCESS',
        data: doc
    }
}

//修改某个用户
const UpdateUser = async (ctx) => { 
    // 拿到用户要删除的id
    let wherestr = {_id: ctx.request.body._id};
    // let updatestr = {
    //     email: ctx.request.body.email,
    //     name: ctx.request.body.name,
    //     phone: ctx.request.body.phone,
    //     sex: ctx.request.body.sex,
    //     password: ctx.request.body.password,
    //     date: ctx.request.body.date,
    // };
    await updateUser(wherestr, ctx.request.body);
    ctx.status = 200;
    ctx.body = {
        retCode: 'SUCCESS',
        message: '修改成功'
    }
}

//删除某个用户
const DelUser = async(ctx) => {
    //拿到要删除的用户id
    let id = ctx.request.body._id;
    await delUser(id);
    ctx.status = 200;
    ctx.body = {
        retCode: 'SUCCESS',
        message: '删除成功'
    }
}
module.exports = {
    Login,
    Reg,
    GetAllUsers,
    UpdateUser,
    DelUser
};