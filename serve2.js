const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

const users = require('./routers/api/users');

const db = require('./config/keys').mongoURI;

//body-parser中间件
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))


mongoose.connect(db, { useNewUrlParser: true })
        .then(() => console.log('MongoDB connected'))
        .catch(err => console.log(err))

app.get('/', (req, res) => { 
    res.send("hello world!");
})

app.use('/api/users',users);


const port = process.env.PORT || 5000;

app.listen(port, () => { 
    console.log(`Server running on port ${port}`);
})